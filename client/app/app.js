(function () {
    // Create angular module "friendsApp" with 2 dependencies "['angularModalService', 'ngAnimate']"
    angular.module("friendsApp", ['angularModalService', 'ngAnimate']);
}());