(function () {
    // Controller's constructor function to attach the controller to the Angular module
    angular.module("friendsApp")
        .controller("ShowUserController", ShowUserController);

    // For minification purposes, the controller needs to be annotated with the $inject property. The $inject property is an array of service names to inject.
    ShowUserController.$inject = ["close", "title"];

    // Controller function
    function ShowUserController(close, title) {
        var vm = this;
        vm.friendDetail = title;

        // Configuration for closing pop up modal
        vm.close = function (result) {
            close(result, 500); // close, but give 500ms for bootstrap to animate

        }
    }



}());